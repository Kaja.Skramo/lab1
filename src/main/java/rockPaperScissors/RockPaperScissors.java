package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;    

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // Implement Rock Paper Scissors

        while(true){
            System.out.println("Let's play round " + roundCounter);

                String humanChoice = humanChoice();
                String computerChoice = computerChoice();

                String choice_string = String.format("Human chose " + humanChoice + ", computer chose " + computerChoice + ".");

                if (humanChoice.equals(computerChoice)){
                    System.out.println(choice_string + " It's a tie!");
                }
                else if (humanChoice.equals("rock")){
                    if (computerChoice.equals("paper")) {
                        System.out.println(choice_string + " Computer wins!");
                        computerScore += 1;
                    }
                    else if (computerChoice.equals("scissors")){
                        System.out.println(choice_string + " Human wins!");
                        humanScore += 1;
                    }
                }
                else if (humanChoice.equals("paper")) {
                    if (computerChoice.equals("scissors")) {
                        System.out.println(choice_string+ " Computer wins!");
                        computerScore += 1;
                    }
                    else if (computerChoice.equals("rock")) {
                        System.out.println(choice_string + " Human wins!");
                        humanScore += 1;
                    }
                }
                else if (humanChoice.equals("scissors")) {
                    if (computerChoice.equals("rock")) {
                        System.out.println(choice_string + " Computer wins!");
                        computerScore += 1;
                    }
                    else if (computerChoice.equals("paper")) {
                        System.out.println(choice_string + " Human wins!");
                        humanScore += 1;
                    }
                }

                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                String continuePlaying = continuePlaying();

                roundCounter += 1;

            if (continuePlaying.equals("n")) {
                break;
           }
       }

       System.out.println("Bye bye :)");
    }

    public String humanChoice() {

        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();


            if(!rpsChoices.contains(humanChoice)) {
                System.out.println("I don't understand " + humanChoice + ". Could you try again");
            }
            else {
                return humanChoice;
            }
        }
    }

    public String computerChoice() {

        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return randomElement;

    }

    public String continuePlaying() {
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();

            if (continueAnswer.equals("y")) {
                return continueAnswer;
            }
            else if (continueAnswer.equals("n")) {
                return continueAnswer;
            }
            else {
                System.out.println("I don't understand " + continueAnswer + ". Try again");
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
